/*
ASPDB

File: debug.c 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include "h/aspdb.h"


void aspdb_dbg_dump_block_read(sHeadP_t *head, sPassData_t *entry, uint32_t id)
{
    PWDM_DEBUG("Print raw entry:\t%u \t%d \t%s \t%s \t%s \t%s \n",
        head->hdbe[id].blockid, entry->passtype, entry->label, head->hdbe[id].label, entry->user, entry->pass);
}


void aspdb_dbg_print_create_archive_custom(char *name, char *generator, uint32_t capacity)
{
    PWDM_DEBUG("New ps archive created\n Name: %s\t Generator: %s\t Size in block: %d\t Header size: %d byte\t Blocksize: %lu bytes\tSize in bytes %lu\n",
    name, generator, capacity, calculate_dynamic_superbloc_size(capacity), sizeof(sPassData_t), 
    (sizeof(sPassData_t)*capacity));
}

void aspdb_dbg_print_memarea_size(void *mem, size_t size, char *text)
{
    unsigned char *p=NULL;

    p=(unsigned char*)mem;
    PWDM_DEBUG("Printing %s at, %p  size %lu\n", text, (void*)p,size );

   
    PWDM_DEBUG("Dumping memory area %s\n", text);
    for(int i=0;i<size;i++)
    {
        if((i%16) == 0)
			PWDM_DEBUG("\n");
        PWDM_DEBUG(" %2hhx ", *p);
        p++;
    }
    PWDM_DEBUG("\nEnd Dumping memory area %s\n", text);    

}