/*
ASPDB

File: aspdb.c 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "h/common.h"
#include "h/data.h"
#include "h/aspdb.h"




void show_usage()
{
    PWDM_PRINT("aspdb help\n");
    PWDM_PRINT("Important: All the options, escluding '-h' require the '-f' option specified\n");
    PWDM_PRINT("\n");
    PWDM_PRINT("\t\t'-c <LABEL>'\t\tCreate a new archive (Ex. \"-c pippo.db\" )\n");
    PWDM_PRINT("\t\t\t'-c' requires the filename with '-f' option and -b\n");
    PWDM_PRINT("\t\t'-b <BN>'\t\tAllow to specify the number of blocks of newly created archive  (Ex. \"-b 1024\" )\n");
    PWDM_PRINT("\t\t'-h'\t\tShow this help\n");
    PWDM_PRINT("\t\t'-f <filename>'\t\tTo specify the db file, MANDATORY (Ex. \"-f pippo.db\" )\n");
    PWDM_PRINT("\t\t'-a'\t\tAdd a new entry interectively\n");
    PWDM_PRINT("\t\t'-w <N>'\t\tShows for entry N and asks for decrypt key (Ex. \"-w 10\")\n");
    PWDM_PRINT("\t\t'-e <N>'\t\tDestroys entry N (Ex. \"-e 10\" )\n");
    PWDM_PRINT("\t\t'-l'\t\tList all the entries in the database\n");
    PWDM_PRINT("\t\t'-s <LABEL>'\t\tSearch entry by label (Ex. \"-s pippo\" )\n");
    PWDM_PRINT("\n\n\t Not Interactive Entry (NOT RECCOMENDED)\n");
    PWDM_PRINT("\t\t'-L <LABEL> -k <KEY> -u <USER> -p <PASSWORD>'  \t\tInsert an entry not interactively \n");
    PWDM_PRINT("\t (Ex. \"-L ziogino -k 12345678 -u Anakin -p pippo\" )");
    PWDM_PRINT("\n\n\t Keep in mind the following current limitation \n");
    PWDM_PRINT("\t\t The maximum label size in characters is %d\n", LABELSIZE);
    PWDM_PRINT("\t\t The minimum size for user is 5 is %d\n", ENTRYSIZE);
    PWDM_PRINT("\t\t The minimum size for password is 5 the maximum is %d\n", ENTRYSIZE);
    PWDM_PRINT("\t\t The minimum size for key is %d maximum is %d\n", MINKEYSIZE,ENTRYSIZE );

}



int check_cli_params_flag(uint8_t flag)
{
    int status=0;

    for(int i=0;i<8;i++)
    {
        if(flag & 1<<i)
            status++;
    }
    return status;
}


int get_strargs(char *optarg, char *str, int maxlen)
{
    int len=0;

    memset(str, 0x00, maxlen);
    if((len=strlen(optarg)) > 0 )
    {           
        if(len>maxlen)
            len=maxlen; 
        strncpy(str,optarg,len);
        str[len]='\0';
        PWDM_DEBUG("%s:%d Optarg %s String %s, len %d, maxlen %d\n", __func__,__LINE__,optarg, str, len, maxlen );
    }
    return len;       
}

int main(int argc, char **argv)
{
    int c, status;
    int flen=0, llen=0,ulen=0, plen=0, klen=0;
    char filename[DBNAME_MAXLEN], label[LABEL_MAXLEN];
    unsigned char user[ENTRYSIZE], password[ENTRYSIZE];
    char key[ENTRYSIZE];
    uint32_t cap=0, entry=0;
    uint8_t flag=0;

// Reading cli parameters

    while ((c = getopt (argc, argv, "cb:af:e:ls:w:vhu:p:k:L:")) != -1)
	{
		switch (c)
        {
			case 'c': flag|=CRT_FLAG; break;
            case 'f': 
            {
                if(!(flen=get_strargs(optarg, filename, DBNAME_MAXLEN)))
                    flag|=ERR_FLAG;
            } break;
            case 'b': 
            {
                if( (cap=(uint32_t)atoi(optarg)) <=0 )
                    flag|=ERR_FLAG;
                      
            } break;
            case 's':
            {
                if(!(llen=get_strargs(optarg, label, LABELSIZE)))
                    flag|=ERR_FLAG;
                else
                    flag|=SRC_FLAG;          
            } break;
            case 'L':
            {
                if(!(llen=get_strargs(optarg, label, LABELSIZE)))
                    flag|=ERR_FLAG;
            } break;
            case 'u': 
            {
                if(!(ulen=get_strargs(optarg, (char*)user, ENTRYSIZE)))
                    flag|=ERR_FLAG;
            } break;
            case 'p': 
            {
                if(!(plen=get_strargs(optarg, (char*)password, ENTRYSIZE)))
                    flag|=ERR_FLAG;
            } break;
            case 'k': 
            {
                if(!(klen=get_strargs(optarg, key, ENTRYSIZE)))
                    flag|=ERR_FLAG;
                else
                    flag|=APP_FLAG;
            } break;
            case 'a': flag|=ADD_FLAG; break;
            case 'l': flag|=LST_FLAG; break;
            case 'v': flag|=HLP_FLAG; break; 
            case 'e': flag|=ERS_FLAG; entry=atoi(optarg); break;
            case 'w': flag|=SHW_FLAG; entry=atoi(optarg); break;
			default:
				show_usage();
        }
	}



    //PWDM_DEBUG("flag %d\t file %s  len %d \tlabel  %s len %d \tentry %d",flag, filename, flen, label, llen, entry);

    //return 0;


    if((check_cli_params_flag(flag)) != 1) 
    {
        PWDM_ERROR("Incopatible %hhx (or no) options selected\n", flag);
        show_usage(); 
        status=ERROR;  
    }
    else
    {
        switch (flag)
        {
            case CRT_FLAG:
            {
                if(flen==0)
                    {
                        PWDM_PRINT("Using default file name %s\n", DBFILE);
                        strcpy(filename, DBFILE);
                    }
                if(cap ==0)
                {
                    PWDM_PRINT("Using default db capacity %d\n", DBCAPTY);
                    cap=DBCAPTY;
                }
                status=aspdb_cli_create_custom(filename, cap);
            } break;
            case ADD_FLAG:
            {
                if(flen==0)
                {
                    show_usage();
                    status=ERROR;
                }
                else
                    status=aspdb_cli_insert_input_new(filename);
            } break;
            case APP_FLAG:
            {
                if(flen==0)
                {
                    show_usage();
                    status=ERROR;
                }
                else
                {
                    if ((ulen!=0) && (plen!=0) && (klen!=0))
                    {
                        PWDM_DEBUG("DEBUG: Filename %s\tLabel %s\tUser %s\tPassword %s\tkey %s\n", filename, label, user, password, key);
                        status=aspdb_cli_insert_param(filename,label,key,user,password);
                    }
                    else
                    {
                        PWDM_ERROR("ERROR: one of the parameter is empty\n User %d, Password %d, Key %d", ulen, plen, klen);
                        status=ERROR;
                    }
                }
            } break;
            case ERS_FLAG:
            {
                if(flen==0)
                {
                    show_usage();
                    status=ERROR;
                }
                else
                {
                    if(entry==0)
                    {
                        show_usage();
                        status=ERROR;
                    }
                    else
                        status=aspdb_cli_delete(filename, entry);
                }
            } break;
            case LST_FLAG:
            {
                if(flen==0)
                {
                    show_usage();
                    status=ERROR;
                }
                else
                    status=aspdb_cli_list(filename);
            } break;
            case SRC_FLAG:
            {
                if(flen==0)
                {
                    show_usage();
                    status=ERROR;
                }
                else
                {
                    if(llen==0)
                    {
                        show_usage();
                        status=ERROR;
                    }
                    else
                    {
                        status=aspdb_cli_search_label(label, filename);
                    }
                }
            }
            case SHW_FLAG:
            {
                if(flen==0)
                {
                    show_usage();
                    status=ERROR;
                }
                else
                {
                    if(entry==0)
                    {
                        show_usage();
                        status=ERROR;
                    }
                    else
                    {
                        status=aspdb_cli_show(filename, entry);
                    }
                }  
            }break;
            case HLP_FLAG:show_usage();break;
        }
    }

    return status;

}



//status=aspdb_cli_insert_input_new(name);


   /* char *cipher;
    char *clear;

    PWDM_DEBUG("Size record %ld\n", sizeof(sPassData_t));
    sPassData_t data;
    insert_newpasswd(&data);
    print_passwd(&data);

    cipher=encrypt((char *)data.pass, "pippo");
    PWDM_DEBUG("Pass encrypted: %s\n", cipher);
    clear=decrypt(cipher, "pippo");
    PWDM_DEBUG("Pass decrypted: %s\n", clear);*/
