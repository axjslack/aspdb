/*
ASPDB

File: aspdb_cli.c
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#include "h/aspdb.h"


int aspdb_cli_search_label(char *label, char *arch)
{
    
    
    int status=STATUS_OK;
    sAsba_desc_t bd;
    sHeadP_t head;
    sSrcRes_t res;
    sPassData_t pass;
    char answ;
    int i=0;
    
    //Open Archive
    status=open_passwdba(&bd, arch, &head);

    if(status==STATUS_OK)
    {
        //Search
        status=search_entry_table(label, &head, &res);
        //read
        if(res.size>0)
        {
            while(i< res.size)
            {
                PWDM_PRINT("Would you like to open entry n. %d of %d labelled %s (y/n/x)\n", i, res.size, head.hdbe[res.indexes[i]].label)
                scanf("%c",&answ);
                switch (answ)
                {
                    case 'y':
                    {
                        asba_read_block(&bd, (void*)&pass, head.hdbe[res.indexes[i]].blockid);
                        aspdb_cli_print_entry(&pass);
                    } break;
                    case 'n':i++;break;
                    case 'x':i=res.size;break;
                    default:
                        i=res.size; break;
                }
            }
        }
        else
        {
            PWDM_ERROR("No entry with laber %s\n", label);
            status=NOENTRY;
        }
        status=close_passwdba(&bd, &head);     
    }
    else
    {
        PWDM_ERROR("Unable to open archive %s\n", arch);
    }

    return status;
}


int aspdb_cli_aes_record_param(sPassData_t *pass, char *label, char *key, unsigned char *user, unsigned char *pasw, uint8_t ptype)
{
    int status=STATUS_OK;
    
    pass->passtype=ptype;
    strcpy((char*)pass->label, label);

    if(strlen(key) < MINKEYSIZE )
    {
        printf("Error Key of at least 8 char needed. Filling new field failed\n");
        status=NOENTRY;
    }
    else
    {
        if((ptype & PT_FULL) || (ptype & PT_UONLY))
        {
            encrypt1(pass->user, user , key); 
        }
        if((ptype & PT_FULL) || (ptype & PT_PONLY))
        {  
            encrypt1(pass->pass, pasw, key);
        }
    }
    return status;
}

int aspdb_cli_insert_param(char *arch, char *label, char *key, unsigned char *user, unsigned char *passw)
{
    int status=STATUS_OK;
    sPassData_t pass;
    sAsba_desc_t bd;
    sHeadP_t head;

    if((status=open_passwdba(&bd, arch, &head))==STATUS_OK)
    {
        if((status=aspdb_cli_aes_record_param(&pass, label, key, user, passw,(PT_AES | PT_FULL))) == STATUS_OK)
        {
            if((status=write_new_entry(&pass, &bd, &head)) == STATUS_OK)
            {
                
                if((status=close_passwdba(&bd, &head)) == STATUS_OK)
                {
                     PWDM_PRINT("Insertion of new entry successful with status %d\n", status);
                }
                else
                    PWDM_ERROR("Unable to close archive %s, status %d\n", arch, status);
            }
            else
                PWDM_ERROR("Impssible to write new entry, exited with status %d\n", status);
        }
        else
            PWDM_ERROR("Error in new record input stage, exited with status %d\n", status);
    }
    else
        PWDM_ERROR("Unable to open archive %s\n", arch);

    return status;
}

int aspdb_cli_insert_input_new(char *arch )
{
    int status=STATUS_OK;
    sPassData_t pass;
    sAsba_desc_t bd;
    sHeadP_t head;

    if((status=open_passwdba(&bd, arch, &head))==STATUS_OK)
    {
        if((status=fill_new_record_input(&pass, (PT_AES | PT_FULL))) == STATUS_OK)
        {
            if((status=write_new_entry(&pass, &bd, &head)) == STATUS_OK)
            {
                
                if((status=close_passwdba(&bd, &head)) == STATUS_OK)
                {
                     PWDM_PRINT("Insertion of new entry successful with status %d\n", status);
                }
                else
                    PWDM_ERROR("Unable to close archive %s, status %d\n", arch, status);
            }
            else
                PWDM_ERROR("Impssible to write new entry, exited with status %d\n", status);
        }
        else
            PWDM_ERROR("Error in new record input stage, exited with status %d\n", status);
    }
    else
        PWDM_ERROR("Unable to open archive %s\n", arch);

    return status;
}


int aspdb_cli_delete(char *arch, uint32_t id)
{
    int status=STATUS_OK;
    sAsba_desc_t bd;
    sHeadP_t head;

    status=open_passwdba(&bd, arch, &head);

    if(status==STATUS_OK)
    {
        if( (status=asba_delete_block(&bd, id)) ==STATUS_OK)
        {
            head.hdbe[id].flg=ENTRYEMPTY;
            status=update_header_sp(&bd, &head);
            status=close_passwdba(&bd, &head); 
        }
                
        else
            status=close_passwdba(&bd, &head); 
    }
    return status;
}



int aspdb_cli_print_entry(sPassData_t *pass)
{
    int status=STATUS_OK;
    char keytmp[ENTRYSIZE];
    unsigned char cuser[ENTRYSIZE], cpassw[ENTRYSIZE];

    /* I know, it is pointless, but valgrind asked for it. It's not my fault */
    memset(cuser, 0x00, ENTRYSIZE);
    memset(cpassw, 0x00, ENTRYSIZE);

    printf("Insert crypt passpword : ");
    disable_term_echo();
    scanf("%s", keytmp);
    enable_term_echo();

    PWDM_DEBUG("Password type %X\t key %s\n", pass->passtype, keytmp);

    if((pass->passtype & PT_FULL) || (pass->passtype & PT_UONLY))
    {
        PWDM_DEBUG("decrypting user string  %s\n",pass->user );
        decrypt1(cuser, pass->user, keytmp);
    
    }
    else
        strcpy((char*)cuser, "EMPTY");
    if((pass->passtype & PT_FULL) || (pass->passtype & PT_PONLY))
    {
        PWDM_DEBUG("decrypting pass string  %s\n",pass->pass );
        decrypt1(cpassw, pass->pass, keytmp);
    }
    else
        strcpy((char*)cpassw, "EMPTY");

    PWDM_PRINT("User: %s \t\tPassword %s\n", (char*)cuser, (char*)cpassw);
    
    return status;
}



int aspdb_cli_show(char *arch, uint32_t id)
{
    int status=STATUS_OK;
    sAsba_desc_t bd;
    sHeadP_t head;
    sPassData_t entry;


    if(( status=open_passwdba(&bd, arch, &head))==STATUS_OK)
    {
       if( head.hdbe[id].flg == ENTRYUSED )
        {
            if((status=asba_read_block(&bd, &entry, id)) == 0)
            {
                status=aspdb_cli_print_entry(&entry);
            }
        }
        status=close_passwdba(&bd, &head); 
    }

    return status;
}

int aspdb_cli_list(char *arch)
{
    int status=STATUS_OK;
    sAsba_desc_t bd;
    sHeadP_t head;
    int i=0;

    
    //Open Archive
    status=open_passwdba(&bd, arch, &head);

    if(status==STATUS_OK)
    {
        
        for(i=bd.hdesc->spblk;i< bd.hdesc->bn;i++)
        {
            if( head.hdbe[i].flg == ENTRYUSED )
                PWDM_PRINT("%d %u %s\n", i, head.hdbe[i].blockid, head.hdbe[i].label);
        }
        status=close_passwdba(&bd, &head);    
    }
    else
    {
        PWDM_ERROR("Unable to open archive %s\n", arch);
    }

    return status;

}
int aspdb_cli_create_custom(char *arch, uint32_t capacity)
{
    return create_passwdba_custom(arch, GENERATOR, capacity);
}
