/*
ASPDB

File: minput.c 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#define _POSIX_SOURCE
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "h/common.h"
#include "h/data.h"
#include "h/aspdb.h"




void disable_term_echo()
{
    struct termios term;
    if (tcgetattr(STDIN_FILENO, &term) != 0)
    {
        PWDM_ERROR("Impossibile to setup terminal. Password could be shown during digit\n");
    }
 
    term.c_lflag &= ~ECHO;
    tcsetattr(fileno(stdin), 0, &term);
}


void enable_term_echo()
{
    struct termios term;

    if (tcgetattr(STDIN_FILENO, &term) != 0)
    {
        PWDM_ERROR("Impossibile to setup terminal. Password could be shown during digit\n");
    }
    term.c_lflag |= ECHO;
    tcsetattr(fileno(stdin), 0, &term);
}

unsigned char* read_field_input(char *fieldname, unsigned char *clear)
{
    printf("Insert %s : \n", fieldname);   
    scanf("%s", clear);

    return clear;
}

unsigned char* read_hidden_field_input(char *fieldname, unsigned char *clear)
{
    
    printf("Insert %s : \n", fieldname);
    disable_term_echo();
    scanf("%s", clear);
    enable_term_echo();

    return clear;
}

int fill_new_clear_record(sPassData_t *pass, uint8_t ptype)
{
    /*Never meant to be here for real... was just used to test */
    return STATUS_OK;
}


int fill_new_aes_record_input(sPassData_t *pass, uint8_t ptype)
{
    int status=STATUS_OK;
    char keytmp[ENTRYSIZE];
    unsigned char cuser[ENTRYSIZE], cpassw[ENTRYSIZE];

    /*Cleaning the memory storage area. Not necessary, but better safe than sorry */
    memset(pass, 0x00, sizeof(sPassData_t));

    pass->passtype=ptype;
    read_field_input("Label", pass->label);
    read_hidden_field_input("Key", (unsigned char*)keytmp);
    if(strlen(keytmp) < MINKEYSIZE )
    {
            printf("Error Key of at least 8 char needed. Filling new field failed\n");
            status=NOENTRY;
    }
    else
    {
        
        if((ptype & PT_FULL) || (ptype & PT_UONLY))
        {
            read_hidden_field_input("Username", cuser);
            //PWDM_DEBUG("Username %s\n", cuser);
            encrypt1(pass->user, cuser , keytmp);
            //PWDM_DEBUG("Encrypted username %s\n", pass->user);
        }
        if((ptype & PT_FULL) || (ptype & PT_PONLY))
        {
            read_hidden_field_input("Password", cpassw);
            encrypt1(pass->pass, cpassw, keytmp);
            //PWDM_DEBUG("Encrypted password %s\n", pass->pass);
        }
    }
    return status;
}


int fill_new_record_input(sPassData_t *pass, uint8_t ptype)
{
    
    //PWDM_DEBUG("%s\t%d: PTYPE %d\n", __func__, __LINE__, ptype);
    pass->passtype=ptype;
    if( ptype & PT_AES)
        return fill_new_aes_record_input(pass, ptype);
    else
        return fill_new_clear_record(pass, ptype);
}




