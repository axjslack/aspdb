/*
ASPDB

File: data.h 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/


#ifndef _DATA_PASSWD_H_
#define _DATA_PASSWD_H_


#include <stdint.h>
#include "h/common.h"

typedef struct sPassData {
    uint8_t passtype;
    unsigned char label[LABELSIZE];
    unsigned char user[ENTRYSIZE]; //It is even too much
    unsigned char pass[ENTRYSIZE];//Unlikely bigger than that.
} sPassData_t;

typedef struct sHeadPdba_ele {
    uint32_t blockid;
    unsigned char label[LABELSIZE];
    uint8_t flg;
    char ext[3]; //Reserved for future use. 
} sHeadPdba_ele_t;

/*typedef struct sSBKTab {
    uint8_t *flag;
} sSBKTab_t;

typedef struct sHeadPba {
    sHeadPdba_ele_t *hdbe;
    sSBKTab_t   tab;
} sHeadPdba_t;

typedef struct sHeadP {
    uint8_t init;
    sHeadPdba_t  m;
} sHeadP_t;
*/

typedef struct sHeadP {
    uint8_t init;
    sHeadPdba_ele_t *hdbe;
} sHeadP_t;


typedef struct sSrcRes {
    uint32_t size;
    uint32_t *indexes;
} sSrcRes_t;

#endif