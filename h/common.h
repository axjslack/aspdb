/*
ASPDB

File: common.h 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#ifndef _COMMON_H_
#define _COMMON_H_


#define ASPDB       01

#define PWDM_PRINT(...) fprintf(stdout, __VA_ARGS__);
#define PWDM_ERROR(...) fprintf(stderr, __VA_ARGS__);
#ifdef DBG
#define PWDM_DEBUG(...) fprintf(stderr, __VA_ARGS__);
#else
#define PWDM_DEBUG(...) 
#endif

#define DBCAPTY             1024
#define MEMORY_SIZE         64
#define DBFILE              "pwd.db"

#define GENERATOR           "ASPDB"


#define STATUS_OK           0

#define ERROR               -1
#define FILE_ERROR          -3
#define NOENTRY             -5

#define LABELSIZE           8
#define ENTRYSIZE           32
#define MINKEYSIZE          8

#define ENTRYEMPTY          0x00
#define ENTRYUSED           0x01
#define ENTRYSP             0x08
#define ENTRYERR            0xFF


#define PT_EMPTY       0x00
#define PT_FULL        0x01
#define PT_PONLY       0x02
#define PT_UONLY       0x04
#define PT_CLEAR       0x40
#define PT_AES         0x80



#define     CRT_FLAG    0x01
#define     ERS_FLAG    0x02
#define     ADD_FLAG    0x04
#define     LST_FLAG    0x08
#define     SHW_FLAG    0x10
#define     SRC_FLAG    0x20
#define     APP_FLAG    0x40
#define     HLP_FLAG    0x80
#define     ERR_FLAG    0xFF

#define     DBNAME_MAXLEN   12
#define     LABEL_MAXLEN    8



#ifndef PASSFILE
#define PASSFILE "pwd.txt"
#endif


#endif