/*
ASPDB

File: aspdb.h
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#ifndef _ASPDB_H_
#define _ASPDB_H_

/* Header file of Alexjan Small Password Database */

#include "h/data.h"
#include "h/common.h"

//#ifndef DIRTYBUILD
//#define DIRTYBUILD
//#endif

#ifdef DIRTYBUILD
#include "../blockarchive/asba.h"
#else
#include "asba.h"
#endif

// search
int search_entry_table(char *pattern, sHeadP_t *head, sSrcRes_t *res);


// Archive 
uint16_t calculate_dynamic_superbloc_size(int size_blk);
int open_passwdba(sAsba_desc_t *bdesc, char *name, sHeadP_t *head);
int create_passwdba_custom (char *name, char *generator, uint32_t capacity);
int delete_entry(sAsba_desc_t *bdesc, sHeadP_t *head,  uint32_t id);
int read_entry(sPassData_t *pass, sAsba_desc_t *bdesc, uint32_t id);
int write_new_entry(sPassData_t *pass, sAsba_desc_t *bdesc, sHeadP_t *head);
int update_header_sp(sAsba_desc_t *bdesc, sHeadP_t *head);
int close_passwdba(sAsba_desc_t *bdesc, sHeadP_t *head);



//Terminal functions
void disable_term_echo();
void enable_term_echo();


// input
int fill_new_aes_record_param(sPassData_t *pass, char *label, char *key, unsigned char *user, unsigned char *pasw, uint8_t ptype);
int fill_new_record_input(sPassData_t *pass, uint8_t ptype);
unsigned char* read_hidden_field_input(char *fieldname, unsigned char *clear);


// Encrypt/Decrypt
void *encrypt1(unsigned char *enc_pass, unsigned char *cleartext, char *key);
void *decrypt1(unsigned char *dec_pass, unsigned char *ciphertext, char *key);


//CLI

int aspdb_cli_list(char *arch);
int aspdb_cli_search_label(char *label, char *arch);
int aspdb_cli_insert_input_new(char *arch );
int aspdb_cli_insert_param(char *arch, char* label, char *key, unsigned char *user, unsigned char *pass);
int aspdb_cli_create_custom(char *arch, uint32_t capacity);
int aspdb_cli_show(char *arch, uint32_t id);
int aspdb_cli_delete(char *arch, uint32_t id);
int aspdb_cli_print_entry(sPassData_t *pass);


//Debug;
void aspdb_dbg_dump_block_read(sHeadP_t *head, sPassData_t *entry, uint32_t id);
void aspdb_dbg_print_create_archive_custom(char *name, char *generator, uint32_t capacity);
void aspdb_dbg_print_memarea_size(void *mem, size_t size, char *text);

#endif